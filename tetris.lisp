;;;; lisp-tetris.lisp
;;;;
;;;; Need to run in sbcl
;;;;    (load "lisp-tetris.lisp")
;;;;    (tetris::tetris)
(ql:quickload :cl-charms)

(defpackage :tetris
  (:use common-lisp cl-charms))

(in-package :tetris)

(defvar *refresh-time* (* internal-time-units-per-second (/ 3 5)))

(defconstant +sleep-time+ .06)

(defconstant *x-max* 10)

(defconstant *y-max* 22)

(defvar *board* (make-array *y-max*)
  "An array of size *y-max* that holds lists with the first element being
another array of size *x-max* (for a row of the board) and the second element
being the number of spaces used in the row.")

(defvar *lines* 0)

(defvar *origin* 24)

(defvar *orientation* 0)

(defvar *curr-shape* nil)

(defvar *curr-tetromino* nil)

(defconstant *shapes* #(#(#(-1 0 1 2); tetromino i
                          #(-10 0 10 20)
                          #(-1 0 1 2)
                          #(-10 0 10 20))
     
                        #(#(-1 0 1 11); tetromino j
                          #(-10 0 10 9)
                          #(-11 0 -1 1)
                          #(-10 0 -9 10))
     
                        #(#(-1 0 1 9); tetromino L
                          #(-10 0 10 11)
                          #(-9 0 -1 1)
                          #(-10 0 -11 10))
     
                        #(#(1 0 11 10); tetromino o
                          #(-10 0 -9 1)
                          #(-10 0 -1 -11)
                          #(-1 0 9 10))
     
                        #(#(1 0 10 9); tetromino s
                          #(-1 0 -11 10)
                          #(1 0 10 9)
                          #(-1 0 -11 10))
     
                        #(#(-1 0 1 10); tetromino t
                          #(-10 0 10 1)
                          #(-1 0 -10 1)
                          #(-1 0 -10 10))
     
                        #(#(-1 0 11 10); tetromino z
                          #(-1 0 -10 9)
                          #(-1 0 11 10)
                          #(-1 0 -10 9))))

(defun set-position (position data)
  "Sets a POSITION to DATA,
   and increments the current pieces if necessary."
  (let* ((x-row (aref *board* (floor position *x-max*)))
         (remainder (rem position *x-max*))
         (curr-data (aref (first x-row) remainder)))
    (cond
      ((and curr-data data)
       (setf (aref (first x-row) remainder) data))
      ((and data (not curr-data))
       (setf (rest x-row) (list (+ (second x-row) 1)))
       (setf (aref (first x-row) remainder) data))
      (t ;(and (not data) curr-data)
       (setf (rest x-row) (list (- (second x-row) 1)))
       (setf (aref (first x-row) remainder) data)))))

(defun into-board (tetromino origin data)
  "Put TETROMINO into the board at ORIGIN with DATA."
  (loop :for offset :across tetromino
        :collect (get-position (+ origin offset))
        :do (set-position (+ origin offset) data)))

(defun initialize-board ()
  "Initialize each element of *board* as described in *board*'s docstring."
  (loop :for i :from 0 :upto (- (length *board*) 1) :do
     (setf (aref *board* i) (list (make-array *x-max* :initial-element nil) 0))))

(defun update-tetromino ()
  "Updates *curr-tetromino* to match the current *orientation*."
  (setf *curr-tetromino* (aref *curr-shape* *orientation*)))

;;; Wrap this in a let and keep track of last tetromino.
(defun get-tetromino ()
  "Gets a new tetromino and updates the orientation."
  (setf *origin* 24
        *orientation* 0
        *curr-shape* (aref *shapes* (random 7)))
  (update-tetromino)
  ;; Check to see if there are any pieces that
  ;; would prevent a tetromino from being placed.
  (if (member nil (loop :for offset :across *curr-tetromino*
                        :collect (listp (get-position (+ *origin* offset)))))
      nil
      (into-board *curr-tetromino* *origin* 'TTT)))

(defun get-position (position)
  "Gets a position from the board."
  (aref (first (aref *board* (floor position *x-max*))) (rem position *x-max*)))

(defmacro with-into-board (&body body)
  "Wraps a body BODY with into-boards that remove the *curr-tetromino*
   and then replace it when done with BODY."
  `(let ((curr-data (gensym)))
     (setf curr-data (get-position *origin*))
     (into-board *curr-tetromino* *origin* nil)
     (prog1
       ,@body
       (into-board *curr-tetromino* *origin* curr-data))))

(defun valid-rotation-p (tetromino origin)
  "Checks if the rotation is valid."
  (loop :for offset :across tetromino
        :minimizing (mod (+ origin offset) *x-max*) :into min
        :maximizing (mod (+ origin offset) *x-max*) :into max
        :when (not (eq (get-position (+ offset origin)) nil))
        :do (return nil)
        :finally (return (if (and (= min 0) (= max (- *x-max* 1)))
                          nil
                          t))))

(defun valid-move (test-origin tetromino)
  "Test the TETROMINO on the TEST-ORIGIN to see if there's a valid move.
   Returns list of what everything tested to.
   The list consists of the following:
   not-valid: not a valid move.
   valid: valid move.
   bottom-of-board: not a valid move, as the bottom of board."
  (loop :for offset :across tetromino
        :for curr-offset :across *curr-tetromino* :collect
        (let ((test-position (+ test-origin offset))
              (curr-position (+ curr-offset *origin*)))
          (cond
            ((<= (* *y-max* *x-max*) test-position) :bottom-of-board); stops at bottom of board.
            ((= (- *x-max* 1) (abs (- (mod test-position *x-max*)  (mod curr-position *x-max*)))) :not-valid); prevents wrapping.
            ((not (eq nil (get-position test-position))) :not-valid); If space is already used.
            (t :valid)))))

(defun valid-move-highest-priority (test-origin tetromino)
  "Takes the output of valid-move and returns
   the highest priority as defined by the following:
   :not-valid > :bottom-of-board > :valid."
  (let ((moves (valid-move test-origin tetromino)))
    (cond
      ((member :not-valid moves :test #'eq) :not-valid)
      ((member :bottom-of-board moves :test #'eq) :bottom-of-board)
      (t :valid))))

(defun descend ()
  "Move *curr-tetromino* down one space if it is a valid move."
  (with-into-board
    (case (valid-move-highest-priority (+ *origin* *x-max*) *curr-tetromino*)
      ((:valid) (incf *origin* *x-max*))
      (t nil))))

(defun horizontal-move (left-or-right)
  "Make a horizontal move of *curr-tetromino* left if
   LEFT-OR-RIGHT is :left, otherwise horizontal move right.
   Only makes a move if it is valid."
  (let ((x (if (eq left-or-right :left) -1 1)))
    (with-into-board
      (if (and (eq :valid (valid-move-highest-priority (+ *origin* x) *curr-tetromino*))
               (valid-rotation-p *curr-tetromino* (+ *origin* x)))
          (incf *origin* x)))))

(defun rotate (up-or-down)
  "Rotate *curr-tetromino* up if UP-OR-DOWN is :up,
   otherwise rotate down. Only rotates on a valid rotation
   as defined by valid-rotation-p."
  (let* ((x (if (eq up-or-down :up) -1 1))
         (tetromino (aref *curr-shape* (mod (+ *orientation* x) 4))))
    (with-into-board
      (when (valid-rotation-p tetromino *origin*)
          (setf *curr-tetromino* tetromino
                *orientation* (mod (+ *orientation* x) 4))))))

(defun get-full-rows ()
  "Get all rows that are full."
  (let ((full-rows (loop :for i :from 2 :to 21 
                         :if (= (second (aref *board* i)) *x-max*)
                         :collect i)))
    (if (not (and (= (second (aref *board* 0)) 0)
                  (= (second (aref *board* 1)) 0)))
        (append '(0) full-rows)
        full-rows)))

(defun remove-full-row (row)
  "Remove ROW from the board, and shift the rows above ROW down."
  (incf *lines*)
  (loop :for row-i :from row :downto 2
        :do (setf (aref *board* row-i) (aref *board* (- row-i 1)))
        :finally (setf (aref *board* 2) (list (make-array *x-max* :initial-element nil) 0))))

(defun dump-board-to-curses ()
  "Print the contents of the board to the screen and refresh the screen."
  (loop :for i :from 0 :to (- *y-max* 3) 
        :for j :from 2 :to (- *y-max* 1)
        :do (progn
              (charms:move-cursor charms:*standard-window* 0 i)
              (loop :for x :across (first (aref *board* j))
                 :do (charms:write-char-at-cursor charms:*standard-window* (if x #\+ #\space)))))
  (charms:write-string-at-point charms:*standard-window* (format nil "Lines: ~d" *lines*) (+ *x-max* 1) 0)
  (charms:refresh-window charms:*standard-window*))

(defun pause ()
  "Pause the current game until p is pressed."
  (charms:disable-non-blocking-mode charms:*standard-window*)
  (loop :while (not (eq #\p (charms:get-char charms:*standard-window*))))
  (charms:enable-non-blocking-mode charms:*standard-window*))

(defun remove-rows ()
  "Remove all full rows in the board and then call get-tetromino."
  (let ((full-rows (get-full-rows)))
    (loop :while full-rows
          :do (progn
                (when (member 0 full-rows) 
                  (return-from remove-rows nil))
                (mapcar #'remove-full-row full-rows)
                (setf full-rows (get-full-rows))))
    (get-tetromino)))

(defun tetris ()
  (let ((last-time 0)
        (curr-time 0))
    (charms:with-curses ()
	  ;; Initialize everything.
      (charms:disable-echoing)
      (charms:enable-raw-input :interpret-control-characters t)
      (charms:enable-non-blocking-mode charms:*standard-window*)
      (charms:clear-window charms:*standard-window*)

	  ;; Initialize game state.
	  (initialize-board)
	  (setf *lines* 0)
      (get-tetromino)
      (charms:refresh-window charms:*standard-window*)
      (setf last-time (get-internal-real-time))
      
      (loop :named main-loop
            :for key := (charms:get-char charms:*standard-window*
                                         :ignore-error t)
            :do (progn
                  (case key
                    ((#\a) (horizontal-move :left))
                    ((#\d) (horizontal-move :right))
                    ((#\w) (rotate :up))
                    ((#\s) (rotate :down))
                    ((#\q) (return-from main-loop))
                    ((#\p) (pause))
                    ((#\space) (loop :while (descend)
                                         :finally (progn
                                                    (let ((rows (remove-rows)))
                                                      (unless rows
                                                        (return-from main-loop)))))))
                  (setf curr-time (get-internal-real-time))
                  (when (or (< *refresh-time* (- curr-time last-time)) (eq key #\space))
                    (when (not (descend))
                      (progn
                        (unless (remove-rows)
                          (return-from main-loop))))
                    (setf last-time (get-internal-real-time)))
                  (dump-board-to-curses)
                  (sleep +sleep-time+)))))
  (format t "Lines: ~D~%" *lines*))
